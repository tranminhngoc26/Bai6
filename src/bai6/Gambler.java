public class Gambler extends Account {

  public Gambler(int balance) {
    super(balance);
  }

  public void withdraw(int money) {
    if (money > 0 && money < (balance * 49 / 100)) {
      balance = balance - money;
      transactions++;
      System.out.println("Giao dich thanh cong");
    } else if (money > 0 && money > (balance * 51 / 100)
        && money + (balance * 51 / 100) < balance) {
      balance = balance - (balance * 51 / 100);
      transactions++;
      System.out.println("Giao dich thanh cong");
    } else {
      System.out.println("Giao dich khong thanh cong");
    }

  }

  public String toString() {
    return "Gambler{" + "balance=" + balance + ", transactions=" + transactions + '}';
  }
}
