
public class TestAccount {

    public static void main(String[] args) {

        Account tk1 = new NormalAccount(2000000);
        tk1.checkAccount();
        System.out.println(tk1.toString());

        tk1.deposit(200000);
        System.out.println(tk1.toString());

        tk1.withdraw(-5000);
        System.out.println(tk1.toString());

        tk1.endMonthCharge();
        System.out.println(tk1.toString());

        System.out.println("=======================");

        Account tk2 = new NickelNDime(4000000);
        tk2.checkAccount();
        System.out.println(tk2.toString());

        tk2.deposit(10000);
        tk2.deposit(25000);
        tk2.withdraw(90000);

        System.out.println(tk2.toString());

        tk2.endMonthCharge();
        System.out.println(tk2.toString());

        System.out.println("========================");

        Account tk3 = new Gambler(450000);
        tk3.checkAccount();
        System.out.println(tk3.toString());

        tk3.withdraw(800000);
        System.out.println(tk3.toString());
    }

}
