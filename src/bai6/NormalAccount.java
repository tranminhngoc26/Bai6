public class NormalAccount extends Account {

    public NormalAccount(int balance) {
        super(balance);
    }

    public void endMonthCharge() {

        balance = balance - 10000;
        System.out.println("Phi giao dich hang thang 10.000");
        

    }

    @Override
    public String toString() {
        return "NormalAccount{" + "balance=" + balance + ", transactions=" + transactions + '}';

    }
}
