public class Account {

    int balance = 0;
    int transactions = 0;

    public Account(int balance) {
        this.balance = balance;
    }

    void checkAccount() {

        if (balance > 0) {
            this.balance = balance;
        } else {
            this.balance = 0;
        }
    }

    public void deposit(int money) {

        if (money > 0) {

            balance = balance + money;
            transactions++;
            System.out.println("Giao dich thanh cong");
        } else {

            System.out.println("Giao dich khong thanh cong");
        }
    }

    public void withdraw(int money) {

        if (money > 0 && money < (balance + 50000)) {

            balance = balance - money;
            transactions++;
            System.out.println("Giao dich thanh cong");
        } else {

            System.out.println("Giao dich khong thanh cong");
        }
    }

    public void endMonthCharge() {

    }

    @Override
    public String toString() {
        return "Account{" + "balance=" + balance + ", transactions=" + transactions + '}';

    }

}
